<?php

/*
 * @file
 * RSN extensions.
 */

/**
 * Default dispatch callback for sending newsletters.
 */
function rsn_rsn_dispatch_default() {

  timer_start(__FUNCTION__);
  $count = 0;

  // Grab the next batch of queued newsletter issues.
  $queues = rsn_dispatch_queue('rsn_rsn_dispatch_default', variable_get('rsn_default_dispatch_batch', 50));

  // Get corresponding subscriptions and issues.
  $issues = array();
  $subscriptions = array();
  foreach ($queues as $queue) {
    $issues[] = $queue->iid;
    $subscriptions[] = $queue->scid;
  }
  $issues = rsn_newsletter_issue_load_multiple($issues);
  $subscriptions = rsn_subscription_load_multiple($subscriptions);

  // Get corresponding subscribers.
  $subscribers = array();
  foreach ($subscriptions as $subscription) {
    $subscribers[] = $subscription->sid;
  }
  $subscribers = rsn_subscriber_load_multiple($subscribers);

  // Iterate through each queue, dispatch newsletter issue.
  foreach ($queues as $queue) {
    $subscription = $subscriptions[$queue->scid];
    $subscriber = $subscribers[$subscription->sid];

    // Send email
    $params = array('issue' => $issues[$queue->iid]);
    drupal_mail('rsn', 'rsn:issue:send', $subscriber->mail, LANGUAGE_NONE, $params);

    // Update queue.
    $queue->status = RSN_QUEUE_SENT;
    $queue->queued = time();
    $queue->save();
    $count++;
  }

  $timer = timer_stop(__FUNCTION__);
  $message = '!count emails dispatched in !time ms.';
  $vars = array(
    '!count' => $count,
    '!time' => $timer['time'],
  );
  watchdog('rsn', $message, $vars);
}

/**
 * Default callback for building a queue to send newsletters.
 */
function rsn_rsn_queue_default($data) {
  $batch = array(
    'title' => t('Building Queue'),
    'operations' => array(
      array('rsn_rsn_queue_default_batch', array($data)),
    ),
    'finished' => 'rsn_rsn_queue_default_batch_finished',
    'file' => drupal_get_path('module', 'rsn') . '/rsn.inc',
  );
  batch_set($batch);
}

/**
 * Batch operation to build queue of issues for sending.
 */
function rsn_rsn_queue_default_batch($data, &$context) {
  if (empty($context['sandbox'])) {
    $context['sandbox']['issue'] = $context['results']['issue'] = $data['issue'];
    $context['sandbox']['newsletter'] = rsn_newsletter_load($context['sandbox']['issue']->nid);
    $context['sandbox']['last_scid'] = 0;
    $context['sandbox']['total'] = db_select('rsn_subscription', 'sc')->fields('sc')
        ->condition('nid', $context['sandbox']['issue']->nid)->countQuery()->execute()->fetchField();
    $context['sandbox']['count'] = 0;
  }

  // Grab a batch of subscriptions to build into queue.
  $scids = db_select('rsn_subscription', 'sc')->fields('sc', array('scid'))
      ->condition('scid', $context['sandbox']['last_scid'], '>')->condition('nid', $context['sandbox']['issue']->nid)
      ->orderBy('scid')->range(0, 20)->execute()->fetchCol();
  $subscriptions = rsn_subscription_load_multiple($scids);

  foreach ($subscriptions as $subscription) {
    // Create a new queue object.
    $queue = rsn_queue_default();
    $queue->iid = $context['sandbox']['issue']->iid;
    $queue->scid = $subscription->scid;
    $queue->status = RSN_QUEUE_PENDING;
    $queue->dispatch = $context['sandbox']['newsletter']->advanced['dispatch'];
    $queue->save();

    // Update some info we're using to track the batch's progress.
    $context['sandbox']['count']++;
    if ($subscription->scid > $context['sandbox']['last_scid']) {
      $context['sandbox']['last_scid'] = $subscription->scid;
    }
  }

  // Report back the progress.
  $vars = array(
    '!count' => $context['sandbox']['count'],
    '!total' => $context['sandbox']['total'],
  );
  $context['message'] = t('Processed subscription !count of !total', $vars);
  $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
}

/**
 * Finished callback for default queue building mechanism.
 */
function rsn_rsn_queue_default_batch_finished($success, $results, $operations) {
  $results['issue']->status = RSN_ISSUE_STATUS_SENDING;
  $results['issue']->save();
}