<?php

/**
 * @file
 * RSN module Views hooks.
 */

/**
 * Implements hook_views_data().
 */
function rsn_views_data() {
  $data = array();

  $data['rsn_newsletter']['table']['group'] = t('Newsletters');
  $data['rsn_newsletter']['table']['base'] = array(
    'field' => 'nid',
    'title' => t('Newsletters'),
    'help' => t('Newsletter data.'),
  );
  $data['rsn_newsletter']['nid'] = array(
    'title' => t('NID'),
    'help' => t('Newsletter ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['rsn_newsletter']['entity_type'] = array(
    'title' => t('Entity type'),
    'help' => t('Entity type of the newsletter.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['rsn_newsletter']['etid'] = array(
    'title' => t('ETID'),
    'help' => t('Entity ID of the newsletter.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['rsn_newsletter']['name'] = array(
    'title' => t('Name'),
    'help' => t('Name of the newsletter.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['rsn_newsletter']['name'] = array(
    'title' => t('Name'),
    'help' => t('Name of the newsletter.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['rsn_newsletter']['status'] = array(
    'title' => t('Status'),
    'help' => t('Status of the newsletter.'),
    'field' => array(
      'handler' => 'views_handler_field_boolean',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Enabled'),
      'type' => 'yes-no',
      'use equal' => TRUE,
    ),
  );
  $data['rsn_newsletter']['subscribe'] = array(
    'title' => t('Subscriptions'),
    'help' => t('Subscription status of the newsletter.'),
    'field' => array(
      'handler' => 'views_handler_field_rsn_newsletter_subscribe',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter',
    ),
  );
  $data['rsn_newsletter']['link'] = array(
    'title' => t('Link'),
    'help' => t('Link to the Newsletter entity.'),
    'field' => array(
      'real field' => 'etid',
      'handler' => 'views_handler_field_rsn_url',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rsn_newsletter_issue']['table']['group'] = t('Newsletter Issues');
  $data['rsn_newsletter_issue']['table']['base'] = array(
    'field' => 'iid',
    'title' => t('Newsletter Issues'),
    'help' => t('Newsletter issue data.'),
  );
  $data['rsn_newsletter_issue']['iid'] = array(
    'title' => t('IID'),
    'help' => t('Issue ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['rsn_newsletter_issue']['entity_type'] = array(
    'title' => t('Entity type'),
    'help' => t('Entity type of the newsletter issue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['rsn_newsletter_issue']['etid'] = array(
    'title' => t('ETID'),
    'help' => t('Entity ID of the newsletter issue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['rsn_newsletter_issue']['nid'] = array(
    'relationship' => array(
      'base' => 'rsn_newsletter',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Newsletter'),
      'title' => t('Newsletter'),
    ),
  );
  $data['rsn_newsletter_issue']['name'] = array(
    'title' => t('Name'),
    'help' => t('Name of the issue.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );
  $data['rsn_newsletter_issue']['status'] = array(
    'title' => t('Status'),
    'help' => t('Newsletter issue status.'),
    'field' => array(
      'handler' => 'views_handler_field_rsn_issue_status',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );
  $data['rsn_newsletter_issue']['link'] = array(
    'title' => t('Link'),
    'help' => t('Link to the newsletter issue entity.'),
    'field' => array(
      'real field' => 'etid',
      'handler' => 'views_handler_field_rsn_url',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
  );

  $data['rsn_subscriber']['table']['group'] = t('Subscribers');
  $data['rsn_subscriber']['table']['base'] = array(
    'field' => 'sid',
    'title' => t('Subscribers'),
    'help' => t('Subscriber data.'),
  );
  $data['rsn_subscriber']['sid'] = array(
    'title' => t('SID'),
    'help' => t('Subscriber ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['rsn_subscriber']['mail'] = array(
    'title' => t('Mail'),
    'help' => t('Subscriber email address.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_string',
    ),
  );

  $data['rsn_subscription']['table']['group'] = t('Subscriptions');
  $data['rsn_subscription']['table']['base'] = array(
    'field' => 'scid',
    'title' => t('Subscriptions'),
    'help' => t('Subscription data.'),
  );
  $data['rsn_subscription']['scid'] = array(
    'title' => t('SCID'),
    'help' => t('Subscription ID.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric',
    ),
  );
  $data['rsn_subscription']['nid'] = array(
    'relationship' => array(
      'base' => 'rsn_newsletter',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Newsletter'),
      'title' => t('Newsletter'),
    ),
  );
  $data['rsn_subscription']['sid'] = array(
    'relationship' => array(
      'base' => 'rsn_subscriber',
      'base field' => 'sid',
      'handler' => 'views_handler_relationship',
      'label' => t('Subscriber'),
      'title' => t('Subscriber'),
    ),
  );
  $data['rsn_subscription']['created'] = array(
    'title' => t('Created'),
    'help' => t('Date subscription was created.'),
    'field' => array(
      'handler' => 'views_handler_field_date',
      'click sortable' => TRUE,
    ),
    'sort' => array(
      'handler' => 'views_handler_sort_date',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_date',
    ),
  );
  $data['rsn_subscription']['delete_link'] = array(
    'title' => t('Delete link'),
    'help' => t('Link to delete the subscription.'),
    'field' => array(
      'real field' => 'scid',
      'handler' => 'views_handler_field_rsn_subscription_delete',
    ),
  );

  return $data;
}
