<?php

class views_handler_field_rsn_issue_status extends views_handler_field {

  function render($values) {
    switch ($this->get_value($values)) {
      case RSN_ISSUE_STATUS_DRAFT:
        return t('Draft');
        break;
      case RSN_ISSUE_STATUS_SENDING:
        return t('Sending');
        break;
      case RSN_ISSUE_STATUS_COMPLETE:
        return t('Complete');
        break;
    }
  }
  
}
