<?php

class views_handler_field_rsn_url extends views_handler_field_url {

  public $entities = array();

  function init(&$view, &$options) {
    parent::init($view, $options);
    $this->additional_fields['entity_type'] = 'entity_type';
  }

  function pre_render(&$values) {
    parent::pre_render($values);
    $this->entities = array();

    // Break down the results by entity_type.
    foreach ($values as $value) {
      $entity_type = $this->get_value($value, 'entity_type');
      $etid = $this->get_value($value);
      $this->entities[$entity_type][] = $etid;
    }

    // Load entities in bulk.
    foreach ($this->entities as $entity_type => &$entities) {
      $entities = entity_load($entity_type, $entities);
    }
  }

  function render($values) {

    $entity_type = $this->get_value($values, 'entity_type');
    $etid = $this->get_value($values);
    $value = entity_uri($entity_type, $this->entities[$entity_type][$etid]);
    $value = array_shift($value);

    if (!empty($this->options['display_as_link'])) {
      $this->options['alter']['make_link'] = TRUE;
      $this->options['alter']['path'] = $value;
      $text = !empty($this->options['text']) ? $this->sanitize_value($this->options['text']) : $this->sanitize_value($value, 'url');
      return $text;
    }
    else {
      return $this->sanitize_value($value, 'url');
    }
  }

}
