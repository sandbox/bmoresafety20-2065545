<?php

class views_handler_field_rsn_subscription_delete extends views_handler_field {

  function init(&$view, &$options) {
    parent::init($view, $options);
  }

  function render($values) {
    $scid = $this->get_value($values);
    return l(t('Delete'), "rsn/subscription/delete/$scid", array('query' => drupal_get_destination()));
  }
}
