<?php

class views_handler_field_rsn_newsletter_subscribe extends views_handler_field {

  function render($values) {
    switch ($this->get_value($values)) {
      case RSN_NEWSLETTER_SUBSCRIBE_CLOSED:
        return t('Closed');
        break;
      case RSN_NEWSLETTER_SUBSCRIBE_OPEN:
        return t('Open');
        break;
    }
  }

}
