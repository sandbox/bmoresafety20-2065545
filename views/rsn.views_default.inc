<?php

/**
 * @file
 * Default views for RSN.
 */

/**
 * Implements hook_views_default_views().
 */
function rsn_views_default_views() {
  $views = array();

$view = new view();
$view->name = 'rsn_newsletter_issues';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'rsn_newsletter_issue';
$view->human_name = 'RSN Newsletter Issues';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['access']['type'] = 'none';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '50';
$handler->display->display_options['pager']['options']['offset'] = '0';
$handler->display->display_options['pager']['options']['id'] = '0';
$handler->display->display_options['pager']['options']['quantity'] = '9';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'link' => 'link',
  'name' => 'name',
  'status' => 'status',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'link' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'name' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'status' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* Relationship: Newsletter Issues: Newsletter */
$handler->display->display_options['relationships']['nid']['id'] = 'nid';
$handler->display->display_options['relationships']['nid']['table'] = 'rsn_newsletter_issue';
$handler->display->display_options['relationships']['nid']['field'] = 'nid';
$handler->display->display_options['relationships']['nid']['required'] = TRUE;
/* Field: Newsletter Issues: Link */
$handler->display->display_options['fields']['link']['id'] = 'link';
$handler->display->display_options['fields']['link']['table'] = 'rsn_newsletter_issue';
$handler->display->display_options['fields']['link']['field'] = 'link';
$handler->display->display_options['fields']['link']['label'] = '';
$handler->display->display_options['fields']['link']['exclude'] = TRUE;
$handler->display->display_options['fields']['link']['element_label_colon'] = FALSE;
/* Field: Newsletter Issues: Name */
$handler->display->display_options['fields']['name']['id'] = 'name';
$handler->display->display_options['fields']['name']['table'] = 'rsn_newsletter_issue';
$handler->display->display_options['fields']['name']['field'] = 'name';
$handler->display->display_options['fields']['name']['label'] = 'Issue';
$handler->display->display_options['fields']['name']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['name']['alter']['path'] = '[link]';
/* Field: Newsletter Issues: Status */
$handler->display->display_options['fields']['status']['id'] = 'status';
$handler->display->display_options['fields']['status']['table'] = 'rsn_newsletter_issue';
$handler->display->display_options['fields']['status']['field'] = 'status';
/* Contextual filter: Newsletters: Entity type */
$handler->display->display_options['arguments']['entity_type']['id'] = 'entity_type';
$handler->display->display_options['arguments']['entity_type']['table'] = 'rsn_newsletter';
$handler->display->display_options['arguments']['entity_type']['field'] = 'entity_type';
$handler->display->display_options['arguments']['entity_type']['relationship'] = 'nid';
$handler->display->display_options['arguments']['entity_type']['default_action'] = 'not found';
$handler->display->display_options['arguments']['entity_type']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['entity_type']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['entity_type']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['entity_type']['summary_options']['items_per_page'] = '25';
$handler->display->display_options['arguments']['entity_type']['limit'] = '0';
/* Contextual filter: Newsletters: ETID */
$handler->display->display_options['arguments']['etid']['id'] = 'etid';
$handler->display->display_options['arguments']['etid']['table'] = 'rsn_newsletter';
$handler->display->display_options['arguments']['etid']['field'] = 'etid';
$handler->display->display_options['arguments']['etid']['relationship'] = 'nid';
$handler->display->display_options['arguments']['etid']['default_action'] = 'not found';
$handler->display->display_options['arguments']['etid']['default_argument_type'] = 'fixed';
$handler->display->display_options['arguments']['etid']['summary']['number_of_records'] = '0';
$handler->display->display_options['arguments']['etid']['summary']['format'] = 'default_summary';
$handler->display->display_options['arguments']['etid']['summary_options']['items_per_page'] = '25';
  $views[$view->name] = $view;

  $view = new view();
  $view->name = 'rsn_newsletter_subscriptions';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'rsn_subscription';
  $view->human_name = 'RSN Newsletter Subscriptions';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'mail' => 'mail',
    'created' => 'created',
    'delete_link' => 'delete_link',
  );
  $handler->display->display_options['style_options']['default'] = 'mail';
  $handler->display->display_options['style_options']['info'] = array(
    'mail' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'delete_link' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Subscriptions: Newsletter */
  $handler->display->display_options['relationships']['nid']['id'] = 'nid';
  $handler->display->display_options['relationships']['nid']['table'] = 'rsn_subscription';
  $handler->display->display_options['relationships']['nid']['field'] = 'nid';
  $handler->display->display_options['relationships']['nid']['required'] = TRUE;
  /* Relationship: Subscriptions: Subscriber */
  $handler->display->display_options['relationships']['sid']['id'] = 'sid';
  $handler->display->display_options['relationships']['sid']['table'] = 'rsn_subscription';
  $handler->display->display_options['relationships']['sid']['field'] = 'sid';
  $handler->display->display_options['relationships']['sid']['required'] = TRUE;
  /* Field: Subscribers: Mail */
  $handler->display->display_options['fields']['mail']['id'] = 'mail';
  $handler->display->display_options['fields']['mail']['table'] = 'rsn_subscriber';
  $handler->display->display_options['fields']['mail']['field'] = 'mail';
  $handler->display->display_options['fields']['mail']['relationship'] = 'sid';
  /* Field: Subscriptions: Created */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'rsn_subscription';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'n/j/Y g:ia';
  /* Field: Subscriptions: Delete link */
  $handler->display->display_options['fields']['delete_link']['id'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['table'] = 'rsn_subscription';
  $handler->display->display_options['fields']['delete_link']['field'] = 'delete_link';
  $handler->display->display_options['fields']['delete_link']['label'] = '';
  $handler->display->display_options['fields']['delete_link']['element_label_colon'] = FALSE;
  /* Contextual filter: Newsletters: Entity type */
  $handler->display->display_options['arguments']['entity_type']['id'] = 'entity_type';
  $handler->display->display_options['arguments']['entity_type']['table'] = 'rsn_newsletter';
  $handler->display->display_options['arguments']['entity_type']['field'] = 'entity_type';
  $handler->display->display_options['arguments']['entity_type']['relationship'] = 'nid';
  $handler->display->display_options['arguments']['entity_type']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['entity_type']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['entity_type']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['entity_type']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['entity_type']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['entity_type']['limit'] = '0';
  /* Contextual filter: Newsletters: ETID */
  $handler->display->display_options['arguments']['etid']['id'] = 'etid';
  $handler->display->display_options['arguments']['etid']['table'] = 'rsn_newsletter';
  $handler->display->display_options['arguments']['etid']['field'] = 'etid';
  $handler->display->display_options['arguments']['etid']['relationship'] = 'nid';
  $handler->display->display_options['arguments']['etid']['default_action'] = 'not found';
  $handler->display->display_options['arguments']['etid']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['etid']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['etid']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['etid']['summary_options']['items_per_page'] = '25';
  $views[$view->name] = $view;

  return $views;
}
