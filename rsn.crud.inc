<?php

/**
 * @file
 * CRUD operations for RSN data.
 */

/**
 * Load a newsletter.
 */
function rsn_newsletter_load($id) {
  $entities = rsn_newsletter_load_multiple(array($id));
  return reset($entities);
}

/**
 * Load multiple newsletters.
 */
function rsn_newsletter_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($conditions) {
    $conditions = rsn_entity_conditions('rsn_newsletter', $conditions);
    $ids = ($ids) ? array_intersect($ids, $conditions) : $conditions;
  }
  $entities = entity_load('rsn_newsletter', $ids, NULL, $reset);
  return $entities;
}

/**
 * Load a subscriber.
 */
function rsn_subscriber_load($id) {
  $entities = rsn_subscriber_load_multiple(array($id));
  return reset($entities);
}

/**
 * Load multiple subscribers.
 */
function rsn_subscriber_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($conditions) {
    $conditions = rsn_entity_conditions('rsn_subscriber', $conditions);
    $ids = ($ids) ? array_intersect($ids, $conditions) : $conditions;
  }
  return entity_load('rsn_subscriber', $ids, NULL, $reset);
}

/**
 * Load a subscription.
 */
function rsn_subscription_load($id) {
  $entities = rsn_subscription_load_multiple(array($id));
  return reset($entities);
}

/**
 * Load multiple subscriptions.
 */
function rsn_subscription_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($conditions) {
    $conditions = rsn_entity_conditions('rsn_subscription', $conditions);
    $ids = ($ids) ? array_intersect($ids, $conditions) : $conditions;
  }
  return entity_load('rsn_subscription', $ids, NULL, $reset);
}

/**
 * Load a newsletter_issue.
 */
function rsn_newsletter_issue_load($id) {
  $entities = rsn_newsletter_issue_load_multiple(array($id));
  return reset($entities);
}

/**
 * Load multiple newsletter_issues.
 */
function rsn_newsletter_issue_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($conditions) {
    $conditions = rsn_entity_conditions('rsn_newsletter_issue', $conditions);
    $ids = ($ids) ? array_intersect($ids, $conditions) : $conditions;
  }
  $issues = entity_load('rsn_newsletter_issue', $ids, NULL, $reset);
  return $issues;
}

/**
 * Load a queue.
 */
function rsn_queue_load($id) {
  $entities = rsn_queue_load_multiple(array($id));
  return reset($entities);
}

/**
 * Load multiple queues.
 */
function rsn_queue_load_multiple($ids = array(), $conditions = array(), $reset = FALSE) {
  if ($conditions) {
    $conditions = rsn_entity_conditions('rsn_queue', $conditions);
    $ids = ($ids) ? array_intersect($ids, $conditions) : $conditions;
  }
  return entity_load('rsn_queue', $ids, NULL, $reset);
}

/**
 * Save a queue.
 */
function rsn_queue_save($entity) {
  entity_save('rsn_queue', $entity);
}

/**
 * Helper function to query by conditions.
 */
function rsn_entity_conditions($entity_type, $conditions) {
  // @todo: some caching here would be nice.
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);
  foreach ($conditions as $column => $value) {
    $query->propertyCondition($column, $value);
  }
  $result = $query->execute();
  return (empty($result[$entity_type])) ? array() : array_keys($result[$entity_type]);
}
