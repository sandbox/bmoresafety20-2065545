<?php

/**
 * @file
 * Forms and form handling utilized by RSN module.
 */

/**
 * Form definition to handle managing subscriptions.
 */
function rsn_manage_subscriptions_form($form, &$form_state, $subscriber) {
  $subscriptions = rsn_subscription_load_multiple(NULL, array('sid' => $subscriber->sid));
  $newsletters = rsn_newsletter_load_multiple(NULL, array('status' => 1));
  $form_state['storage']['subscriber'] = $subscriber;
  $form_state['storage']['subscriptions'] = $subscriptions;
  $form_state['storage']['newsletters'] = $newsletters;

  $form = array();

  // Build list of newsletters.
  $form['newsletters'] = array('#tree' => TRUE);
  foreach ($newsletters as $newsletter) {
    $form['newsletters'][$newsletter->nid]['name'] = array(
      '#title' => t('Newsletter'),
      '#markup' => l($newsletter->name, "{$newsletter->entity_type}/{$newsletter->etid}"),
    );
    $form['newsletters'][$newsletter->nid]['subscribe'] = array(
      '#type' => 'checkbox',
      '#title' => t('Subscribe'),
    );
  }

  // Set default values.
  foreach ($subscriptions as $subscription) {
    $form['newsletters'][$subscription->nid]['subscribe']['#default_value'] = TRUE;
  }

  // Make sure newsletters with subscriptions closed can only be unsubscribed from.
  foreach ($newsletters as $newsletter) {
    if (!$newsletter->subscribe && empty($form['newsletters'][$newsletter->nid]['subscribe']['#default_value'])) {
      $form['newsletters'][$newsletter->nid]['subscribe'] = array(
        '#type' => 'value',
        '#value' => FALSE,
      );
    }
  }

  $form['actions'] = array(
    '#type' => 'actions',
    '#weight' => 100,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 *
 */
function rsn_manage_subscriptions_form_validate($form, &$form_state) {
  $subscriptions = $form_state['storage']['subscriptions'];
  $subscriber = $form_state['storage']['subscriber'];

  // Gather newsletter ids for existing subscriptions.
  $nids = array();
  foreach ($subscriptions as $subscription) {
    $nids[$subscription->nid] = $subscription;
  }

  // Analyze desired subscriptions, make sure they all exist.
  foreach ($form_state['values']['newsletters'] as $nid => $newsletter) {
    if ($newsletter['subscribe'] && !array_key_exists($nid, $nids)) {
      $subscription = rsn_subscription_default();
      $subscription->nid = $nid;
      $subscription->sid = $subscriber->sid;
      $nids[$nid] = $subscription;
    }
  }

  $form_state['storage']['subscriptions'] = $nids;
}

/**
 * Submit callback for manage subscriptions form.
 */
function rsn_manage_subscriptions_form_submit($form, &$form_state) {
  $subscriber = $form_state['storage']['subscriber'];
  $subscriptions = $form_state['storage']['subscriptions'];
  $newsletters = $form_state['storage']['newsletters'];

  // Iterate through newsletters, delete subscriptions as necessary.
  foreach ($form_state['values']['newsletters'] as $nid => $vals) {
    if (!$vals['subscribe']) {
      // No subscription indicated, delete existing if needed.
      if (!empty($subscriptions[$nid])) {
        $subscriptions[$nid]->delete();
        unset($subscriptions[$nid]);
      }
    }
    else {
      $subscriptions[$nid]->save();
    }
  }

  drupal_set_message(t('Your subscription settings have been saved.'));
}

/**
 * Form used to send tests and dispatch a newsletter issue.
 */
function rsn_newsletter_issue_dispatch_form($form, &$form_state, $issue) {

  $form_state['storage']['issue'] = $issue;
  $form_state['storage']['newsletter'] = rsn_newsletter_load($issue->nid);

  $form = array();

  $form['issue'] = array(
    '#type' => 'fieldset',
    '#title' => t('Send Newsletter Issue'),
  );

  $form['issue']['recipient'] = array(
    '#type' => 'radios',
    '#title' => t('Deliver to'),
    '#options' => array(
      RSN_ISSUE_RECIPIENT_TEST => t('Test recipient'),
      RSN_ISSUE_RECIPIENT_ALL => t('All subcribers')
    ),
    '#default_value' => RSN_ISSUE_RECIPIENT_TEST,
  );

  $form['issue']['test_recipient'] = array(
    '#type' => 'textfield',
    '#title' => t('Test Recipient'),
    '#description' => t('Enter an address to send a test copy of the newsletter issue to.  Multiple addresses may be separated by commas, or semicolons.'),
  );

  $note = (empty($issue->content)) ?
      t('<b>No test issues have been sent.</b><br/>If you dispatch this issue to all subscribers, issue content will be generated at this time.') :
      t('<b>A test issue has been sent.</b><br/>If you dispatch this issue to all subscribers the email generated from your last test will be used.  If you would like to update the email contents, manipulate the issue as desired and send another test.');
  $form['issue']['content'] = array(
    '#type' => 'container',
    'note' => array('#markup' => $note),
    '#attributes' => array('class' => array('alert alert-info')),
  );

  $form['issue']['send'] = array(
    '#type' => 'submit',
    '#value' => t('Send Newsletter Issue'),
  );

  return $form;
}

/**
 * Validation callback for rsn_newsletter_issue_dispatch_form.
 */
function rsn_newsletter_issue_dispatch_form_validate(&$form, &$form_state) {
  // Check test addresses.
  if ($form_state['values']['recipient'] == RSN_ISSUE_RECIPIENT_TEST) {

    // Explode the string on commas and semicolons.
    $data = explode(',', $form_state['values']['test_recipient']);
    foreach ($data as $key => $item) {
      $data[$key] = explode(';', $item);
    }

    // Flatten array. Also trimming whitespace and filter for unique values.
    $emails = array();
    foreach ($data as $items) {
      foreach ($items as $item) {
        $emails[] = trim($item);
      }
    }
    $emails = array_unique($emails);

    foreach ($emails as $email) {
      if (!valid_email_address($email)) {
        form_set_error('test_recipient', t('Test newsletter issues may only be sent to valid email addresses.'));
        return;
      }
    }

    $form_state['values']['test_recipient'] = $emails;
  }
}

/**
 * Submit callback for sn_newsletter_issue_dispatch_form().
 */
function rsn_newsletter_issue_dispatch_form_submit(&$form, &$form_state) {
  global $language;

  $issue = $form_state['storage']['issue'];

  // Take action appropriate to the operation indicated in the form.
  switch ($form_state['values']['recipient']) {
    case RSN_ISSUE_RECIPIENT_TEST:
      $issue->content = rsn_mail_generate(array('issue' => $issue));
      $issue->save();
      $params = array('issue' => $issue);
      foreach ($form_state['values']['test_recipient'] as $mail) {
        drupal_mail('rsn', 'rsn:issue:test', $mail, LANGUAGE_NONE, $params);
      }
      drupal_set_message(t('Test issue dispatched to !mails', array('!mails' => implode(', ', $form_state['values']['test_recipient']))));
      break;
    case RSN_ISSUE_RECIPIENT_ALL:
      // Invoke the desired queue extension to prep the newsletter for sending.
      $newsletter = rsn_newsletter_load($issue->nid);
      // @todo: fix serial params.
      $issue->data['send_time'] = time();
      if (empty($issue->content)) {
        $issue->content = rsn_mail_generate(array('issue' => $issue));
      }
      // @todo: fix invoking extension.
      rsn_extension_invoke('queue', $newsletter->queue, array('issue' => $issue));
      $issue->save();
      break;
  }
}

/**
 * Form definition to allow a user to subscribe to a newsletter.
 */
function rsn_newsletter_subscribe_form($form, &$form_state, $newsletter = NULL) {
  static $newsletters;

  $form = array();

  // Set entity data for use during validation/submission.
  $form_state['storage']['newsletter'] = $newsletter;

  $form['mail'] = array(
    '#type' => 'textfield',
    '#required' => TRUE,
    '#attributes' => array('placeholder' => t('Email address')),
  );

  // If no newsletter specified, display options to subscribe to all.
  if (!$newsletter) {
    // Simple caching mechanism.
    if (!$newsletters) {
      $newsletters = db_select('rsn_newsletter', 'n')->fields('n', array('nid', 'name'))
              ->orderBy('name')->execute()->fetchAllKeyed();
    }
    $form['newsletters'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Newsletters'),
      '#options' => $newsletters,
      '#default_value' => array_keys($newsletters),
    );
  }

  $form['quiet'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not send welcome message'),
    '#access' => user_access('manage rsn newsletters'),
  );

  $form['actions'] = array('#weight' => 100);
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
  );

  return $form;
}

/**
 * Validation callback for rsn_newsletter_subscribe_form().
 */
function rsn_newsletter_subscribe_form_validate(&$form, &$form_state) {
  // Clean up and validate the email field.
  $form_state['values']['mail'] = trim($form_state['values']['mail']);
  if (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t('Please enter a valid email address.'));
  }

  // If the user specified a certain newsletter, we'll check to make sure
  // they're not already subscribed. Otherwise we'll sign them up for
  // everything they requested.
  $newsletter = $form_state['storage']['newsletter'];
  $conditions = array('mail' => $form_state['values']['mail']);
  $subscriber = rsn_subscriber_load_multiple(NULL, $conditions);
  $subscriber = reset($subscriber);
  $form_state['storage']['subscriber'] = $subscriber;

  if ($newsletter) {
    if ($subscriber) {
      $conditions = array('nid' => $newsletter->nid, 'sid' => $subscriber->sid);
      $subscription = rsn_subscription_load_multiple(NULL, $conditions);
      $subscription = reset($subscription);
      if ($subscription) {
        $vars = array('!link' => l('newsletter management page', 'rsn/manage'));
        form_set_error('mail', t('A subscription already exists for this email address. Please use the !link to adjust your preferences.', $vars));
      }
    }
  }
  else {
    if (!count(array_filter($form_state['values']['newsletters']))) {
      form_set_error('newsletters', t('Please select at least one newsletter to subscribe.'));
    }
  }
}

/**
 * Submit callback for rsn_newsletter_subscribe_form().
 */
function rsn_newsletter_subscribe_form_submit(&$form, &$form_state) {
  $newsletter = $form_state['storage']['newsletter'];

  // Load subscriber, create new one if necessary.
  if (!$form_state['storage']['subscriber']) {
    $subscriber = rsn_subscriber_default();
    $subscriber->mail = $form_state['values']['mail'];
    $subscriber->save();
  }
  else {
    $subscriber = $form_state['storage']['subscriber'];
  }

  // If a specific newsletter was requested.
  if ($newsletter) {
    // Create a new subscription.
    $subscription = rsn_subscription_default();
    $subscription->sid = $subscriber->sid;
    $subscription->nid = $newsletter->nid;
    $subscription->save();
  }
  else {
    // Create subscriptions for all requested newsletters.
    $subscriptions = rsn_subscription_load_multiple(NULL, array('sid' => $subscriber->sid));
    $nids = array();
    foreach ($subscriptions as $subscription) {
      $nids[] = $subscription->nid;
    }
    foreach (array_filter($form_state['values']['newsletters']) as $nid) {
      if (!array_key_exists($nid, $nids)) {
        $subscription = rsn_subscription_default();
        $subscription->sid = $subscriber->sid;
        $subscription->nid = $nid;
        $subscription->save();
      }
    }
  }

  // Notification.
  drupal_set_message(t('Your subscription has been saved.'));
}

/**
 * Form definition for a user to request a new link to manage his/her subscriptions.
 */
function rsn_send_manage_link_form($form, &$form_state) {
  $form = array();

  $form['mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Email address'),
    '#required' => TRUE,
    '#description' => t('Enter your email address to receive a link to manage your newsletter subscriptions.'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Send'),
  );

  return $form;
}

/**
 * Form definition for a user to request a new link to manage his/her subscriptions.
 */
function rsn_send_manage_link_form_validate(&$form, &$form_state) {
  // Make sure the email is legit.
  if (!valid_email_address($form_state['values']['mail'])) {
    form_set_error('mail', t('Please enter a valid email address.'));
  }

  // Load an existing subscriber if one exists for this email address.
  $conditions = array('mail' => $form_state['values']['mail']);
  $subscriber = rsn_subscriber_load_multiple(NULL, $conditions);
  $subscriber = reset($subscriber);
  if ($subscriber) {
    $form_state['storage']['subscriber'] = $subscriber;
  }
}

/**
 * Form definition for a user to request a new link to manage his/her subscriptions.
 */
function rsn_send_manage_link_form_submit(&$form, &$form_state) {
  // Create the subscriber if needed.
  if (empty($form_state['storage']['subscriber'])) {
    $subscriber = rsn_subscriber_default();
    $subscriber->mail = $form_state['values']['mail'];
  }
  else {
    // Reset the subscriber's hash.
    $subscriber = $form_state['storage']['subscriber'];
  }

  rsn_subscription_manage_url($subscriber);
  $url = url(rsn_subscription_manage_url($subscriber), array('absolute' => TRUE));
  $link = l($url, $url);
  $params['subscriber'] = $subscriber;
  drupal_mail('rsn', 'rsn:subscriber:send-link', $subscriber->mail, LANGUAGE_NONE, $params);
  $variables = array(':mail' => $subscriber->mail, '@link' => $link, '@mail' => $subscriber->mail);
  drupal_set_message(t('A link to manage your subscriptions has been sent to :mail.', $variables));
  watchdog('rsn', 'Subscription management link sent to @mail: @link', $variables);
}

/**
 * Form definition for deleting a subscription.
 */
function rsn_subscription_delete_form($form, &$form_state, $subscription) {
  $form = array();
  $form_state['storage']['subscription'] = $subscription;
  $subscriber = rsn_subscriber_load($subscription->sid, 'sid');
  $newsletter = rsn_newsletter_load($subscription->nid);
  $args = array(':mail' => $subscriber->mail, ':newsletter' => $newsletter->name);
  $question = t('Are you sure you want to delete :mail\'s subscription to the newsletter :newsletter?', $args);
  $path = isset($_GET['destination']) ? $_GET['destination'] : '';
  return confirm_form($form, $question, $path);
}

/**
 * Submit callback for subscription delete form.
 */
function rsn_subscription_delete_form_submit(&$form, &$form_state) {
  $form_state['storage']['subscription']->delete();
  drupal_set_message(t('The subscription has been deleted.'));
}

/**
 * Add RSN newsletter configuration form configuration into entity forms.
 */
function rsn_entity_attach_newsletter_form(&$form, &$form_state, $entity, $entity_type) {

  $newsletter = (!empty($entity->rsn_newsletter)) ? $entity->rsn_newsletter : NULL;

  $form['rsn_newsletter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter Configuration'),
    '#tree' => TRUE,
  );

  $form['rsn_newsletter']['status'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enabled'),
    '#description' => t('Enable this node for use as a newsletter.'),
    '#default_value' => FALSE,
  );

  // Set default values.
  if ($newsletter) {
    $form['rsn_newsletter']['status']['#default_value'] = $newsletter->status;
  }

  drupal_alter('rsn_newsletter_form', $form['rsn_newsletter']);
}

/**
 * Add RSN newsletter_issue configuration form configuration into entity forms.
 */
function rsn_entity_attach_newsletter_issue_form(&$form, &$form_state, $entity, $entity_type) {
  $issue = (!empty($entity->rsn_newsletter_issue)) ? $entity->rsn_newsletter_issue : NULL;

  $form['rsn_newsletter_issue'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletter Issue'),
    '#tree' => TRUE,
    '#access' => user_access('manage rsn newsletters'),
  );

  $form['rsn_newsletter_issue']['nid'] = array(
    '#type' => 'select',
    '#title' => t('Newsletter'),
    '#options' => rsn_newsletter_options(),
    '#empty_option' => '- ' . t('Select') . ' -',
  );

  // Set default values.
  if ($issue) {
    $form['rsn_newsletter_issue']['nid']['#default_value'] = $issue->nid;
    // Only allow this to be changed when working with issues in draft status.
    if ($issue->status != RSN_ISSUE_STATUS_DRAFT) {
      $form['rsn_newsletter_issue']['nid']['#disabled'] = TRUE;
    }
  }

  drupal_alter('rsn_newsletter_issue_form', $form['rsn_newsletter_issue']);
}

/**
 * Load a list of enabled newsletters for use in select/radio options.
 */
function rsn_newsletter_options() {
  static $options;

  if (!$options) {
    $options = db_select('rsn_newsletter', 'n')->fields('n', array('nid', 'name'))
            ->condition('status', 1)->execute()->fetchAllKeyed();
  }

  return $options;
}
