<?php

/**
 * @file
 * Entity hooks for RSN module.
 */

/**
 * Implements hook_entity_info().
 */
function rsn_entity_info() {
  return array(
    'rsn_newsletter' => array(
      'label' => t('Newsletter'),
      'class' => 'SeEntity',
      'controller class' => 'SeEntityController',
      'module' => 'rsn',
      'base table' => 'rsn_newsletter',
      'entity keys' => array(
        'id' => 'nid',
        'label' => 'name',
      ),
    ),
    'rsn_subscriber' => array(
      'label' => t('Newsletter Subscriber'),
      'class' => 'SeEntity',
      'controller class' => 'SeEntityController',
      'module' => 'rsn',
      'base table' => 'rsn_subscriber',
      'entity keys' => array(
        'id' => 'sid',
        'label' => 'mail',
      ),
    ),
    'rsn_subscription' => array(
      'label' => t('Newsletter Subscription'),
      'class' => 'SeEntity',
      'controller class' => 'SeEntityController',
      'module' => 'rsn',
      'base table' => 'rsn_subscription',
      'entity keys' => array(
        'id' => 'scid',
      ),
    ),
    'rsn_newsletter_issue' => array(
      'label' => t('Newsletter Issue'),
      'class' => 'SeEntity',
      'controller class' => 'SeEntityController',
      'module' => 'rsn',
      'base table' => 'rsn_newsletter_issue',
      'entity keys' => array(
        'id' => 'iid',
        'label' => 'name',
      ),
    ),
    'rsn_queue' => array(
      'label' => t('Queued Newsletter Issue'),
      'class' => 'SeEntity',
      'controller class' => 'SeEntityController',
      'module' => 'rsn',
      'base table' => 'rsn_queue',
      'entity keys' => array(
        'id' => 'qid',
      ),
    ),
  );
}

/**
 * Implements hook_entity_info_alter().
 *
 * Add the "Newsletter Issue" view mode to nodes.
 */
function rsn_entity_info_alter(&$entity_info) {
  $entity_info['node']['view modes']['rsn_issue'] = array('label' => t('Newsletter Issue'), 'custom settings' => FALSE);
}

/**
 * Implements hook_entity_load().
 *
 * When entities are loaded, attach their RSN Newsletter and RSN Newsletter
 * Issue entities.
 */
function rsn_entity_load($entities, $entity_type) {

  $conditions = array('entity_type' => $entity_type, 'etid' => array_keys($entities));

  // Attempt to load rsn_newsletter data for the entities.
  $newsletters = rsn_newsletter_load_multiple(NULL, $conditions);
  foreach ($newsletters as $newsletter) {
    list($etid, $vid, $bundle) = entity_extract_ids($entity_type, $entities[$newsletter->etid]);
    if (rsn_is_newsletter_type($bundle, $entity_type)) {
      $entities[$newsletter->etid]->rsn_newsletter = $newsletter;
    }
  }

  $newsletter_issues = rsn_newsletter_issue_load_multiple(NULL, $conditions);
  foreach ($newsletter_issues as $newsletter_issue) {
    list($etid, $vid, $bundle) = entity_extract_ids($entity_type, $entities[$newsletter_issue->etid]);
    if (rsn_is_newsletter_issue_type($bundle, $entity_type)) {
      $entities[$newsletter_issue->etid]->rsn_newsletter_issue = $newsletter_issue;
    }
  }
}

/**
 * Implements hook_entity_view().
 *
 * Add the subscribe form via the "extra field".
 */
function rsn_entity_view($entity, $entity_type, $view_mode, $langcode) {

  // Add subscribe form if entity is configured properly.
  if (!empty($entity->rsn_newsletter) && $entity->rsn_newsletter->status && $entity->rsn_newsletter->subscribe == RSN_NEWSLETTER_SUBSCRIBE_OPEN) {
    $entity->content['rsn_newsletter'] = array(
      '#type' => 'fieldset',
      '#title' => t('Subscribe'),
      drupal_get_form('rsn_newsletter_subscribe_form', $entity->rsn_newsletter),
    );
  }
}

/**
 * Save an entity's RSN data.
 */
function rsn_entity_save($entity, $entity_type) {

  list($etid, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  // Save newsletter data.
  if (rsn_is_newsletter_type($bundle, $entity_type) && !empty($entity->rsn_newsletter)) {
    $newsletter = (!empty($entity->original->rsn_newsletter)) ? $entity->original->rsn_newsletter : rsn_newsletter_default();
    $newsletter->setProperties($entity->rsn_newsletter);
    $newsletter->etid = $etid;
    $newsletter->entity_type = $entity_type;
    $newsletter->name = entity_label($entity_type, $entity);
    $newsletter->save();
    $entity->rsn_newsletter = $newsletter;
  }

  // Save newsletter issue data.
  if (rsn_is_newsletter_issue_type($bundle, $entity_type) && !empty($entity->rsn_newsletter_issue)) {
    $newsletter_issue = (!empty($entity->original->rsn_newsletter_issue)) ? $entity->original->rsn_newsletter_issue : rsn_newsletter_issue_default();
    $newsletter_issue->setProperties($entity->rsn_newsletter_issue);
    $newsletter_issue->etid = $etid;
    $newsletter_issue->entity_type = $entity_type;
    $newsletter_issue->name = entity_label($entity_type, $entity);
    if (!empty($newsletter_issue->nid)) {
      $newsletter_issue->save();
      $entity->rsn_newsletter_issue = $newsletter_issue;
    }
    else {
      $newsletter_issue->delete();
      unset($entity->rsn_newsletter_issue);
    }
  }
}

/**
 * Implements hook_entity_insert().
 */
function rsn_entity_insert($entity, $entity_type) {
  rsn_entity_save($entity, $entity_type);

  // Fire off the subscription welcome email.
  if ($entity_type == 'rsn_subscription' && (!isset($entity->quiet) || $entity->quiet == FALSE)) {
    // Get the subscriber.
    $subscriber = rsn_subscriber_load($entity->sid);
    rsn_subscription_manage_url($subscriber);
    $newsletter = rsn_newsletter_load($entity->nid);
    $params = array(
      'newsletter' => $newsletter,
      'subscriber' => $subscriber,
    );
    drupal_mail('rsn', 'rsn:subscriber:subscribe', $subscriber->mail, LANGUAGE_NONE, $params);
  }
}

/**
 * Implements hook_entity_update().
 */
function rsn_entity_update($entity, $entity_type) {
  rsn_entity_save($entity, $entity_type);
}

/**
 * Implements hook_entity_delete().
 */
function rsn_entity_delete($entity, $entity_type) {
  list($etid, $vid, $bundle) = entity_extract_ids($entity_type, $entity);

  // Delete newsletters assigned to the entity.
  $newsletters = rsn_newsletter_load_multiple(NULL, array('etid' => $etid, 'entity_type' => $entity_type));
  foreach ($newsletters as $newsletter) {
    $newsletter->delete();
  }

  // Delete newsletter_issues assigned to the entity.
  $issues = rsn_newsletter_issue_load_multiple(NULL, array('etid' => $etid, 'entity_type' => $entity_type));
  foreach ($issues as $issue) {
    $issue->delete();
  }
}

/**
 * Default instantiation for a rsn_newsletter object.
 */
function rsn_newsletter_default() {
  $newsletter = new SeEntity('rsn_newsletter');
  $newsletter->subscribe = RSN_NEWSLETTER_SUBSCRIBE_CLOSED;
  return $newsletter;
}

/**
 * Default instantiation for a rsn_newsletter_issue object.
 */
function rsn_newsletter_issue_default() {
  $issue = new SeEntity('rsn_newsletter_issue');
  $issue->data = array();
  return $issue;
}

/**
 * Default instantiation for a rsn_subscriber object.
 */
function rsn_subscriber_default() {
  $subscriber = new SeEntity('rsn_subscriber');
  $subscriber->hash = '';
  $subscriber->timestamp = 0;
  return $subscriber;
}

/**
 * Default instantiation for a rsn_subscription object.
 */
function rsn_subscription_default() {
  $subscription = new SeEntity('rsn_subscription');
  $subscription->created = REQUEST_TIME;
  return $subscription;
}

/**
 * Default instantiation for a rsn_subscriber object.
 */
function rsn_queue_default() {
  $queue = new SeEntity('rsn_queue');
  return $queue;
}
