<?php

/**
 * @file
 * Page callbacks for admin screens.
 */

/**
 * Page callback to handle general configurations.
 */
function rsn_admin_config_page() {
  $form = array();

  $form['mail'] = array(
    '#type' => 'fieldset',
    '#title' => t('Mail Configuration'),
  );

  $form['mail']['rsn_default_sender'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#default_value' => variable_get('rsn_default_sender'),
  );

  $form['mail']['rsn_default_reply'] = array(
    '#type' => 'textfield',
    '#title' => t('Reply To'),
    '#default_value' => variable_get('rsn_default_reply'),
  );

  $form['mail']['rsn_default_return'] = array(
    '#type' => 'textfield',
    '#title' => t('Return Path'),
    '#default_value' => variable_get('rsn_default_return'),
  );

  $form['dispatcher'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dispatcher'),
  );

  $link = 'rsn/cron/' . variable_get('cron_key');
  $form['dispatcher']['rsn_default_dispatch_cron'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use standard Drupal cron mechanism'),
    '#description' => t('This setting determines if mail dispatch should occur as a part of the standard Drupal
      cron task. Mail dispatching can be done independantly from Drupal\'s cron by setting up a periodic call to :link .', array(':link' => url($link, array('absolute' => TRUE)))),
    '#default_value' => variable_get('rsn_default_dispatch_cron', FALSE),
  );

  $form['dispatcher']['rsn_default_dispatch_batch'] = array(
    '#type' => 'select',
    '#title' => t('Batch size'),
    '#default_value' => variable_get('rsn_default_dispatch_batch', 50),
    '#options' => array(50 => 50, 100 => 100, 200 => 200, 500 => 500, 1000 => 1000),
  );

  $form['misc'] = array(
    '#type' => 'fieldset',
    '#title' => t('Misc'),
  );

  return system_settings_form($form);
}

/**
 * Page callback to handle content type configurations.
 */
function rsn_admin_content_types_page() {
  $form = array();

  $form['node-types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node Types'),
  );

  $form['node-types']['rsn_newsletter_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Newsletters'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('rsn_newsletter_node_types', array()),
  );

  $form['node-types']['rsn_newsletter_issue_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Newsletter Issues'),
    '#options' => node_type_get_names(),
    '#default_value' => variable_get('rsn_newsletter_issue_node_types', array()),
  );

  return system_settings_form($form);
}

/**
 * Form to allow site administrator to set custom messages for various user emails.
 */
function rsn_admin_message_page() {
  $form = array();

  $form['rsn_subscription_welcome_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription Welcome Message'),
  );

  $form['rsn_subscription_welcome_message']['rsn_subscription_welcome_message_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#default_value' => variable_get('rsn_subscription_welcome_message_subject'),
  );

  $form['rsn_subscription_welcome_message']['rsn_subscription_welcome_message_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#default_value' => variable_get('rsn_subscription_welcome_message_body'),
  );

  $form['rsn_subscription_mgmt_link_reset'] = array(
    '#type' => 'fieldset',
    '#title' => t('Subscription Management Link Reset'),
  );

  $form['rsn_subscription_mgmt_link_reset']['rsn_subscription_mgmt_link_reset_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => variable_get('rsn_subscription_mgmt_link_reset_title'),
  );

  $form['rsn_subscription_mgmt_link_reset']['rsn_subscription_mgmt_link_reset_body'] = array(
    '#type' => 'textarea',
    '#title' => t('body'),
    '#default_value' => variable_get('rsn_subscription_mgmt_link_reset_body'),
  );

  $form['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['tokens']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('rsn_newsletter', 'rsn_subscriber'),
  );

  return system_settings_form($form);
}

/**
 * Form for use to import subcribers and set subscriptions.
 */
function rsn_admin_import_subscribers() {
  $form = array();

  $form['newsletters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Newsletters'),
    '#description' => t('Choose which newsletters to subscribe imported subscribers into.'),
  );

  $form['newsletters'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Newsletters'),
    '#description' => t('Choose which newsletters to subscribe imported subscribers into.'),
    '#options' => rsn_newsletter_options(),
  );

  $form['subscribers'] = array(
    '#type' => 'textarea',
    '#title' => t('Email Addresses'),
  );

  $form['quiet'] = array(
    '#type' => 'checkbox',
    '#title' => t('Do not send welcome message'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );

  return $form;
}

/**
 * Validation handler for rsn_admin_import_subscribers form.
 */
function rsn_admin_import_subscribers_validate(&$form, &$form_state) {

  // Check and preload newsletters.
  $nids = array_filter($form_state['values']['newsletters']);
  if (!count($nids)) {
    form_set_error('newsletters', t('Select at least one newsletter for subscribing.'));
  }
  else {
    $form_state['storage']['newsletters'] = rsn_newsletter_load_multiple($nids);
  }

  // Parse textarea to find individual email addresses.
  $mails = array();
  $lines = explode("\n", $form_state['values']['subscribers']);
  foreach ($lines as $line) {
    $commas = explode(',', $line);
    foreach ($commas as $comma) {
      $items = explode(';', $line);
      foreach ($items as $item) {
        $item = trim($item);
        if (valid_email_address($item) && !in_array($item, $mails)) {
          $mails[] = $item;
        }
      }
    }
  }
  $form_state['storage']['mails'] = $mails;
  if (!count($mails)) {
    form_set_error('subscribers', t('Enter at least one valid email address.'));
  }
}

/**
 * Submit handler for rsn_admin_import_subscribers form.
 */
function rsn_admin_import_subscribers_submit(&$form, &$form_state) {

  $batch = array(
    'title' => t('Importing Subscribers'),
    'operations' => array(
      array('rsn_admin_import_subscribers_batch', array($form_state['storage']['mails'], $form_state['storage']['newsletters'], $form_state['values']['quiet'])),
    ),
    'finished' => 'rsn_admin_import_subscribers_finished',
    'file' => drupal_get_path('module', 'rsn') . '/rsn.admin.inc',
  );
  batch_set($batch);
}

/**
 * Batch operation for importing subscriptions.
 */
function rsn_admin_import_subscribers_batch($mails, $newsletters, $quiet, &$context) {
  if (empty($context['sandbox']['mails'])) {
    $context['sandbox']['mails'] = $mails;
    $context['sandbox']['count'] = count($mails);
    $context['results']['subscribers'] = 0;
    $context['results']['subscriptions'] = 0;
  }

  // Grab the next email for import.
  $mail = array_shift($context['sandbox']['mails']);
  $context['message'] = t('!progress of !count (!mail)', array('!progress' => $context['sandbox']['count'] - count($context['sandbox']['mails']),
    '!count' => $context['sandbox']['count'], '!mail' => check_plain($mail)));

  // Load the subscriber, create one if none exists.
  $subscriber = reset(rsn_subscriber_load_multiple(array(), array('mail' => $mail)));
  if (!$subscriber) {
    $subscriber = rsn_subscriber_default();
    $subscriber->mail = $mail;
    $context['results']['subscribers']++;
    $subscriber->save();
  }

  // Load this subscriber's subscriptions.
  $subscriptions = rsn_subscription_load_multiple(array(), array('sid' => $subscriber->sid));
  $scids = array();
  foreach ($subscriptions as $subscription) {
    $scids[$subscription->nid] = $subscription->scid;
  }

  // Create missing subscriptions.
  foreach ($newsletters as $newsletter) {
    if (!array_key_exists($newsletter->nid, $scids)) {
      $subscription = rsn_subscription_default();
      $subscription->nid = $newsletter->nid;
      $subscription->sid = $subscriber->sid;
      $subscription->quiet = TRUE;
      $subscription->quiet = $quiet;
      $subscription->save();
      $context['results']['subscriptions']++;
    }
  }

  // Set progress indicator.
  if ($context['sandbox']['count']) {
    $context['finished'] = 1 - count($context['sandbox']['mails']) / $context['sandbox']['count'];
  }
}

/**
 * Finished callback for importing subscriptions.
 */
function rsn_admin_import_subscribers_finished($success, $results, $operations) {
  if (!$success) {
    drupal_set_message(t('There was an error importing subscribers.'), 'error');
    return;
  }

  $args = array(
    ':sub' => $results['subscribers'],
    ':scr' => $results['subscriptions'],
  );
  drupal_set_message(t('Imported :sub subscribers and :scr subscriptions.', $args));
}
