<?php

/**
 * @file
 * Hooks provided by RSN Service.
 */

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Define RSN service methods.
 *
 * This hook enables modules to register additional methods which are serviced
 * by the RSN Service endpoint.
 *
 * @return array
 *  An array of methods, keyed by the method name.
 */
function hook_rsn_service_methods() {

}

/**
 * @} End of "addtogroup hooks".
 */
