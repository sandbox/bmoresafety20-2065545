<?php

/**
 * @file
 * Really Simple News Service page callbacks.
 */

/**
 * Page callback for RSN Service endpoint.
 *
 * Analyzes the requests, dispatches to appropriate callback.
 */
function rsn_service_service_endpoint($method = NULL) {

  drupal_page_is_cacheable(FALSE);

  $methods = rsn_service_methods();
  $params = drupal_get_query_parameters();
  $response = array('status' => 200);

  // Figure out which method is requested.
  if (!$method && !empty($params['method'])) {
    $method = $params['method'];
  }

  // Not able to determine method.
  if (!$method) {
    $response['status'] = 404;
    $response['error_message'] = t('Method not provided.');
  } else {
    $response['method'] = $method;
  }

  // Make sure method has been declared by extentions an that its callback exists.
  if (array_key_exists($method, $methods)) {
    // Get the [module_name].rsn_service.inc include loaded.
    $method_info = $methods[$method];
    module_load_include('rsn_service.inc', $method_info['module'], $method_info['module']);
    // If the callback is available, do it. Otherwise error gracefully.
    if (function_exists($method_info['callback'])) {
      call_user_func_array($method_info['callback'], array(&$response, $params));
    } else {
      // No function with that name exists.
      $response['status'] = 404;
      $response['error_message'] = t('Method callback does not exist.');
    }
  } else {
    if ($method) {
      $response['status'] = 404;
      $response['error_message'] = t('Method not declared by RSN Service extensions.');
    }
  }

  // Add the appropriate header for errors.
  switch ($response['status']) {
    case 403:
      drupal_add_http_header('Status', '400 Bad Request');
      break;
    case 403:
      drupal_add_http_header('Status', '403 Forbidden');
      break;
    case 404:
      drupal_add_http_header('Status', '404 Not Found');
      break;
  }

  return $response;
}
