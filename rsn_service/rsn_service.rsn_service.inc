<?php

/**
 * Implements hook_rsn_service_methods().
 */
function rsn_service_rsn_service_methods() {
  return array(
    'newsletter' => array(
      'info' => 'Newsletters',
      'callback' => 'rsn_service_newsletter_service',
    ),
  );
}

/**
 * RSN Service endpoint callback.
 *
 * Newsletter querying and manipulation.
 */
function rsn_service_newsletter_service(&$request, $parameters) {

  $parameters += array('action' => 'list');
  $request['action'] = $parameters['action'];

  switch ($parameters['action']) {
    case 'view':
      rsn_service_newsletter_service_view($request, $parameters);
      break;
    case 'subscribe':
      rsn_service_newsletter_service_subscribe($request, $parameters);
      break;
    default:
      rsn_service_newsletter_service_list($request, $parameters);
  }
}

/**
 * RSN Service endpoint callback.
 *
 * List all newsletters.
 */
function rsn_service_newsletter_service_list(&$request, $parameters) {
  $nids = db_query('SELECT nid, name FROM {rsn_newsletter}')->fetchAll();
  $request['data'] = $nids;
}

/**
 * Add subscribers to newsletters.
 */
function rsn_service_newsletter_service_subscribe(&$request, $parameters) {

  if (empty($parameters['subscriptions'])) {
    $request['status'] = 400;
    $request['error_message'] = t('No subscription data supplied.');
    return;
  }

  // Preload newsletters.
  $newsletters = rsn_newsletter_load_multiple(array_keys($parameters['subscriptions']));

  // Check each newsletter, create subscription if necessary.
  foreach ($parameters['subscriptions'] as $nid => $mails) {
    $exists = in_array($nid, array_keys($newsletters));
    if ($exists) {
      $open = ($newsletters[$nid]->status == RSN_NEWSLETTER_SUBSCRIBE_OPEN);
    }

    if ($exists && $open) {

      foreach ($mails as $mail) {

        if (valid_email_address($mail)) {

          // Lookup existing subscriber, otherwise create one.
          $sid = db_query('SELECT sid FROM {rsn_subscriber} WHERE mail = :mail', array(':mail' => $mail))->fetchField();
          if (!$sid || !$subscriber = rsn_subscriber_load($sid)) {
            $subscriber = rsn_subscriber_default();
            $subscriber->mail = $mail;
            $subscriber->save();
          }
          // Lookup existing subscription, otherwise create one.
          $args = array(
            ':nid' => $nid,
            ':sid' => $subscriber->sid,
          );
          $scid = db_query('SELECT scid FROM {rsn_subscription} WHERE sid = :sid AND nid = :nid', $args)->fetchField();
          if (!$scid || !$subscription = rsn_subscription_load($scid)) {
            $subscription = rsn_subscription_default();
            $subscription->nid = $nid;
            $subscription->sid = $subscriber->sid;
            $subscription->save();
          }
        }
      }
    }
  }

  $request['data'] = t('Success');
}

/**
 * RSN Service endpoint callback.
 *
 * Provide information about a newsletter.
 */
function rsn_service_newsletter_service_view(&$request, $parameters) {

  // Make sure we've got the parameters needed to do a subscription.
  if (empty($parameters['nid']) || !$newsletter = rsn_newsletter_load($parameters['nid'])) {
    $request['status'] = 400;
    $request['error_message'] = t('Invalid newsletter.');
    return;
  }

  // Load attached entity.
  $entities = entity_load($newsletter->entity_type, array($newsletter->etid));
  if (!$entities || !$entity = reset($entities)) {
    $request['status'] = 400;
    $request['error_message'] = t('Unable to load newsletter attached entity.');
    return;
  }

  if (!$entities = entity_load($newsletter->entity_type, array($newsletter->etid)) || !$entity = reset($entities)) {
    $request['status'] = 400;
    $request['error_message'] = t('Unable to load newsletter attached entity.');
    return;
  }

  $uri = entity_uri($newsletter->entity_type, $entity);

  $data = array(
    'nid' => $newsletter->nid,
    'name' => $newsletter->name,
    'url' => url($uri['path'], array('absolute' => TRUE)),
  );

  $request['data'] = $data;
}
