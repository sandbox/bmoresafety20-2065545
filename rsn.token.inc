<?php

/**
 * @file
 * Code used to integrate with Token.
 */

/**
 * Worker function for hook_token_info().
 */
function _rsn_token_info() {

  $types['rsn_newsletter'] = array(
    'name' => t('Newsletters'),
    'description' => t('Tokens related to RSN Newsletters.'),
    'needs-data' => 'rsn_newsletter',
  );
  $tokens['rsn_newsletter']['link'] = array(
    'name' => t('Link'),
    'description' => t('A link to the newsletter.'),
  );
  $tokens['rsn_newsletter']['name'] = array(
    'name' => t('Name'),
    'description' => t('The name of the newsletter.'),
  );

  $types['rsn_subscriber'] = array(
    'name' => t('Subscribers'),
    'description' => t('Tokens related to RSN Subscribers.'),
    'needs-data' => 'rsn_subscriber',
  );
  $tokens['rsn_subscriber']['mail'] = array(
    'name' => t('Mail'),
    'description' => t('Email address of the subscriber.'),
  );
  $tokens['rsn_subscriber']['management-link'] = array(
    'name' => t('Subscription management link'),
    'description' => t('A link where the user can manage subscriptions.'),
  );

  return array(
    'types' => $types,
    'tokens' => $tokens,
  );
}

/**
 * Worker function for hook_tokens().
 */
function _rsn_tokens($type, $tokens, $data, $options) {
  switch ($type) {
    case 'rsn_newsletter':
      return rsn_tokens_rsn_newsletter($tokens, $data, $options);
      break;
    case 'rsn_subscriber':
      return rsn_tokens_rsn_subscriber($tokens, $data, $options);
      break;
  }
}

/**
 * Generate token data for RSN Newsletters.
 */
function rsn_tokens_rsn_newsletter($tokens, $data) {
  $replacements = array();
  $newsletter = $data['rsn_newsletter'];

  foreach ($tokens as $name => $original) {
    switch ($name) {
      case 'link':
        $entity = entity_load($newsletter->entity_type, array($newsletter->etid));
        $entity = reset($entity);
        $uri = entity_uri($newsletter->entity_type, $entity);
        $replacements[$original] = url($uri['path'], array('absolute' => TRUE));
        break;
      case 'name':
        $replacements[$original] = $newsletter->name;
        break;
    }
  }

  return $replacements;
}

/**
 * Generate token data for RSN Subscribers.
 */
function rsn_tokens_rsn_subscriber($tokens, $data) {
  $replacements = array();
  $subscriber = $data['rsn_subscriber'];

  foreach ($tokens as $name => $original) {
    switch ($name) {
      case 'mail':
        $replacements[$original] = $subscriber->mail;
        break;
      case 'management-link':
        $replacements[$original] = url("rsn/manage/{$subscriber->sid}/{$subscriber->hash}", array('absolute' => TRUE));
        break;
    }
  }

  return $replacements;
}
