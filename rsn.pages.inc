<?php

/**
 * @file
 * Page callbacks to support general user operations.
 */

/**
 * Page callback for subscribing to newsletters.
 */
function rsn_newsletters_page() {
  $form = drupal_get_form('rsn_newsletter_subscribe_form');
  drupal_alter('rsn_newsletters_page', $form);
  return $form;
}

/**
 * Page callback to render newsletter issues.
 */
function rsn_newsletter_issues_page($entity_id, $type) {

  // Generate the view, alterable.
  $view = array(
    'name' => 'rsn_newsletter_issues',
    'display' => 'default',
  );
  drupal_alter('rsn_newsletter_issues_view', $view);
  $content['view']['#markup'] = views_embed_view($view['name'], $view['display'], $type, $entity_id);

  return $content;
}

/**
 * Form constructor for rsn_newsletter_config_page().
 */
function rsn_newsletter_config_page($form, &$form_state, $etid, $entity_type) {

  // Get the newsletter entity which is attached to this entity.
  $entities = entity_load($entity_type, array($etid));
  $entity = reset($entities);
  $newsletter = $entity->rsn_newsletter;
  $form_state['newsletter'] = $newsletter;

  $form = array();

  $form['subscribe'] = array(
    '#type' => 'radios',
    '#title' => t('Subscriptions'),
    '#description' => t('Allow users to subscribe to this newsletter. Users may unsubscribe from closed newsletters.'),
    '#default_value' => $newsletter->subscribe,
    '#options' => array(
      RSN_NEWSLETTER_SUBSCRIBE_OPEN => t('Open'),
      RSN_NEWSLETTER_SUBSCRIBE_CLOSED => t('Closed'),
    ),
  );

  $form['addresses'] = array(
    '#type' => 'fieldset',
    '#title' => t('Addresses'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['addresses']['sender_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Sender'),
    '#description' => t('Issues will send from this address.'),
    '#default_value' => $newsletter->sender_mail,
  );

  $form['addresses']['reply_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Reply-to'),
    '#description' => t('Recipients who reply to an issue will send to this address.'),
    '#default_value' => $newsletter->reply_mail,
  );

  $form['addresses']['return_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Return'),
    '#description' => t('Bounce notifications will be directed to this address.'),
    '#default_value' => $newsletter->return_mail,
  );

  $form['extensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Extensions'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['extensions']['queue'] = array(
    '#type' => 'select',
    '#title' => t('Queue'),
    '#options' => array(),
    '#default_value' => $newsletter->queue,
    '#empty_option' => '- ' . t('Default') . ' -',
  );

  $form['extensions']['dispatch'] = array(
    '#type' => 'select',
    '#title' => t('Dispatch'),
    '#options' => array(),
    '#default_value' => $newsletter->dispatch,
    '#empty_option' => '- ' . t('Default') . ' -',
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Submit callback for rsn_newsletter_config_page.
 */
function rsn_newsletter_config_page_submit($form, &$form_state) {
  $newsletter = $form_state['newsletter'];
  $newsletter->setProperties($form_state['values']);
  $newsletter->save();
  drupal_set_message(t('Newsletter configuration saved.'));
}

/**
 * Form constructor for rsn_newsletter_messages_page().
 */
function rsn_newsletter_messages_page($form, &$form_state, $etid, $entity_type) {

  // Get the newsletter entity which is attached to this entity.
  $entities = entity_load($entity_type, array($etid));
  $entity = reset($entities);
  $newsletter = $entity->rsn_newsletter;
  $form_state['newsletter'] = $newsletter;

  $form = array();

  $form['welcome'] = array(
    '#type' => 'fieldset',
    '#title' => t('Welcome message'),
    '#collapsible' => TRUE,
  );

  $form['welcome']['welcome_subject'] = array(
    '#type' => 'textfield',
    '#title' => t('Subject'),
    '#description' => t('Enter the subject for the message to send to subcribers when they join the newsletter.'),
    '#default_value' => $newsletter->welcome_subject,
  );

  $form['welcome']['welcome_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Body'),
    '#description' => t('Enter a message to send to subcribers when they join the newsletter.'),
    '#default_value' => $newsletter->welcome_body,
  );

  $form['tokens'] = array(
    '#type' => 'fieldset',
    '#title' => t('Replacement patterns'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['tokens']['token_tree'] = array(
    '#theme' => 'token_tree',
    '#token_types' => array('rsn_newsletter', 'rsn_subscriber'),
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Submit callback for rsn_newsletter_messages_page.
 */
function rsn_newsletter_messages_page_submit(&$form, &$form_state) {
  $newsletter = $form_state['newsletter'];
  $newsletter->setProperties($form_state['values']);
  $newsletter->save();
  drupal_set_message(t('Newsletter message configuration saved.'));
}

/**
 * Page callback to render newsletter subscriptions.
 */
function rsn_newsletter_subscriptions_page($entity_id, $type) {

  // Load the newsletter entity.
  $entities = entity_load($type, array($entity_id));
  $entity = reset($entities);

  // Generate the subscribe form.
  $content['form'] = array(
    '#type' => 'fieldset',
    '#title' => t('Add Subscriptions')
  );
  $content['form']['form'] = drupal_get_form('rsn_newsletter_subscribe_form', $entity->rsn_newsletter);

  // Generate the view, alterable.
  $view = array(
    'name' => 'rsn_newsletter_subscriptions',
    'display' => 'default',
  );
  drupal_alter('rsn_newsletter_subscriptions_view', $view);
  $content['view']['#markup'] = views_embed_view($view['name'], $view['display'], $type, $entity_id);

  return $content;
}

/**
 * Page callback for newsletter issue sending/summary page.
 */
function rsn_newsletter_issue_page($entity, $entity_type) {

  // Generate output based on newsletter issue status.
  switch ($entity->rsn_newsletter_issue->status) {
    case RSN_ISSUE_STATUS_DRAFT:
      return drupal_get_form('rsn_newsletter_issue_dispatch_form', $entity->rsn_newsletter_issue);
      break;
    case RSN_ISSUE_STATUS_SENDING:
    case RSN_ISSUE_STATUS_COMPLETE:
      return rsn_newsletter_issue_summary_page($entity->rsn_newsletter_issue);
      break;
  }

  // If we get here, something went wrong.
  drupal_not_found();
}

/**
 * Page callback to display markup generated by the issue.
 */
function rsn_newsletter_issue_draft_page($node) {
  return rsn_mail_generate(array('issue' => $node->rsn_newsletter_issue));
}

/**
 * Generates a newsletter issue summary page, with status on sending, etc.
 */
function rsn_newsletter_issue_summary_page($issue) {
  $output['fieldset'] = array(
    '#type' => 'fieldset',
  );

  $rows = array();
  switch ($issue->status) {
    case RSN_ISSUE_STATUS_SENDING:
      $output['fieldset']['#title'] = t('Newsletter sending in progress');

      // Query counts on issue status.
      $count = array(RSN_QUEUE_PENDING => 0, RSN_QUEUE_QUEUED => 0, RSN_QUEUE_SENT => 0, 'total' => 0);
      $data = db_query('SELECT status, COUNT(*) AS cnt FROM rsn_queue
        WHERE iid = :iid GROUP BY status ORDER BY status', array(':iid' => $issue->iid));
      foreach ($data as $row) {
        $count[$row->status] = $row->cnt;
        $count['total'] += $row->cnt;
      }

      // Only compute percentages when we have at least one queued subscriber.
      if ($count['total']) {
        $complete = round($count[RSN_QUEUE_SENT] / $count['total'] * 100);
        $rows[] = array(t('Status'), t('Sending (!sent%)', array('!sent' => $complete)));
      }
      else {
        $rows[] = array(t('Status'), t('Sending (no subscribers queued)'));
      }

      // Build rows for our table.
      $rows[] = array(t('Dispatched at'), format_date($issue->data['send_time']));
      $rows[] = array(t('Pending Recipients'), $count[RSN_QUEUE_PENDING]);
      $rows[] = array(t('Queued Recipients'), $count[RSN_QUEUE_QUEUED]);
      $rows[] = array(t('Sent Recipients'), $count[RSN_QUEUE_SENT]);
      $rows[] = array(t('Total Recipients'), $count['total']);
      break;
    case RSN_ISSUE_STATUS_COMPLETE:
      $output['fieldset']['#title'] = t('Newsletter sending complete');
      // Build rows for our table.
      $rows[] = array(t('Status'), t('Complete'));
      $rows[] = array(t('Dispatched at'), format_date($issue->data['send_time']));
      $rows[] = array(t('Completed at'), format_date($issue->data['complete_time']));
      $rows[] = array(t('Total Recipients'), $issue->data['total_recipients']);
      break;
  }

  $output['fieldset']['table'] = array(
    '#theme' => 'table',
    '#rows' => $rows,
  );

  return $output;
}

/**
 * Page callback to handle newsletter subcription management.
 */
function rsn_manage_page($sid = NULL, $hash = NULL) {

  // Make sure we don't cahce this page.
  drupal_page_is_cacheable(FALSE);

  // No subscriber indicated, present the form to get a reset link.
  if (!$sid) {
    return drupal_get_form('rsn_send_manage_link_form');
  }

  // Take a shot at loading a subscriber.
  $subscriber = rsn_subscriber_load($sid);

  // If a valid subscriber link was not used, set a message and provide a form for the user to reset and send a new link.
  if (!$subscriber || !rsn_subscriber_hash_is_valid($subscriber, $hash)) {
    drupal_set_message(t('This link is not valid.  Please reqeust a new link to manage your subscriptions.'), 'error');
    return drupal_get_form('rsn_send_manage_link_form');
  }

  drupal_set_title(t('Manage subscriptions: !mail', array('!mail' => $subscriber->mail)));
  return drupal_get_form('rsn_manage_subscriptions_form', $subscriber);
}

/**
 * Page callback for RSN cron handler.
 */
function rsn_cron_page() {
  rsn_extension_invoke('dispatch', 'rsn_rsn_dispatch_default', array());
  return t('RSN cron tasks handled successfully.');
}
